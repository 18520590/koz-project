package com.koz.common.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public enum ExceptionEnum {
    ACCOUNT_ALREADY_EXISTS("001", "Username Already Exists", HttpStatus.CONFLICT),
    USER_NOT_FOUND("002", "User not found", HttpStatus.NOT_FOUND),
    EMAIL_OR_USERNAME_EXISTS("003", "Email or username has already exists", HttpStatus.BAD_REQUEST),
    LOGIN_FAIL("401", "Login fail, please check email or password", HttpStatus.UNAUTHORIZED),

    // Unknown Error
    INVALID_REQUEST_BODY("400", "Invalid request body", HttpStatus.BAD_REQUEST),
    NOT_FOUND("404", "Resource not found", HttpStatus.NOT_FOUND),
    ENTITY_NOT_FOUND("4000", "Resource not found", HttpStatus.NOT_FOUND),
    UNKNOWN_ERROR("500", "Unknown error", HttpStatus.INTERNAL_SERVER_ERROR),
    INTERNAL_SERVICE_CALL_EXCEPTION("5000", "Fail to process request!", HttpStatus.INTERNAL_SERVER_ERROR)
    ;

    private final String code;
    private final HttpStatus status;
    private final String description;

    ExceptionEnum(String code, String description, HttpStatus statusCode) {
        this.code = code;
        this.status = statusCode;
        this.description = description;
    }
}
