package com.koz.common.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Date;

@Getter
@Setter
public class ApiException extends RuntimeException {
    private final String code;
    private final HttpStatus status;
    private final Date timestamp;
    private final String description;

    public ApiException(ExceptionEnum exceptionEnum){
        this.code = exceptionEnum.getCode();
        this.status = exceptionEnum.getStatus();
        this.timestamp = new Date();
        this.description = exceptionEnum.getDescription();
    }

    public ApiException(ExceptionEnum exceptionEnum, String msg){
        super(msg);
        this.code = exceptionEnum.getCode();
        this.status = exceptionEnum.getStatus();
        this.timestamp = new Date();
        this.description = exceptionEnum.getDescription();
    }

    public ApiException(ExceptionEnum exceptionEnum, String msg, Throwable cause){
        super(msg, cause);
        this.code = exceptionEnum.getCode();
        this.status = exceptionEnum.getStatus();
        this.timestamp = new Date();
        this.description = msg;
    }
}
