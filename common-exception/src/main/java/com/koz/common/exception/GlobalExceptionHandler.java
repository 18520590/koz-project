package com.koz.common.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.*;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        Map<String, Object> body = new HashMap<>();
        List<FieldError> fieldErrors = new ArrayList<>();
        for(var fieldError : ex.getFieldErrors()){
            fieldErrors.add(new FieldError(fieldError.getField(), fieldError.getDefaultMessage()));
        }
        body.put("message", ExceptionEnum.INVALID_REQUEST_BODY.getDescription());
        body.put("errors", fieldErrors);

        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<ErrorResponse> apiExceptionHandler(ApiException exception){
        log.error("Api exception: {}", exception.getMessage(), exception);
        ErrorResponse errorResponse = new ErrorResponse(exception);
        return new ResponseEntity<>(errorResponse, errorResponse.getStatus());
    }

    @ExceptionHandler(InternalServiceCallException.class)
    public ResponseEntity<ErrorResponse> internalServiceCallException(InternalServiceCallException exception) {
        log.error("Internal service call exception: {}", exception.getMessage(), exception);
        ApiException apiException = new ApiException(ExceptionEnum.INTERNAL_SERVICE_CALL_EXCEPTION);
        ErrorResponse errorResponse = new ErrorResponse(apiException);
        return new ResponseEntity<>(errorResponse, errorResponse.getStatus());
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    public static class FieldError {
        private String field;
        private String message;
    }

    @Getter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ErrorResponse {
        private final String code;
        private final HttpStatus status;
        private final Date timestamp;
        private final String description;

        @Setter
        private List<HashMap<String, String>> errors;
        public ErrorResponse(ApiException apiException){
            this.code = apiException.getCode();
            this.status = apiException.getStatus();
            this.timestamp = apiException.getTimestamp();
            this.description = apiException.getDescription();
        }
    }
}