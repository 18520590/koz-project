package com.koz.common.exception;

public class InternalServiceCallException extends RuntimeException {

    public InternalServiceCallException() {
        super();
    }
    public InternalServiceCallException(String msg) {
        super(msg);
    }
    public InternalServiceCallException(String msg, Throwable cause){
        super(msg, cause);
    }
}
