package com.koz.common.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PagingDtoV2<T>{
    private long total;
    private String lastId;
    private List<T> data;
}
