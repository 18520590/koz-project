package com.koz.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class UserDto {
    private String id;
    private String username;
    private String email;
    @JsonProperty("fName")
    private String fName;
    @JsonProperty("lName")
    private String lName;
    @JsonProperty("dfUrl")
    private String pfUrl;
    private Instant createdAt;
    private Instant updatedAt;
    private Instant lUpTime;
}
