package com.koz.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisterDto {
    private RegisterDto(){}

    @Getter
    @Setter
    public static class Request {
        @Pattern(regexp = "^[a-zA-Z0-9]([._-](?![._-])|[a-zA-Z0-9]){3,18}[a-zA-Z0-9]$",
        message = "The username invalid format")
        @NotBlank
        private String username;

        @Email(message = "The email is invalid format")
        @NotBlank
        private String email;

        @Size(min = 6, max = 32, message = "The password must from 6 - 32 characters")
        @NotBlank
        private String password;

        @Size(min = 1, max = 32, message = "The first name is required and must from 1 - 32")
        @JsonProperty("fName")
        @NotBlank
        private String fName;

        @Size(min = 1, max = 32, message = "The last name is required and must from 1 - 32")
        @JsonProperty("lName")
        @NotBlank
        private String lName;
    }

    @Getter
    @Setter
    public static class Response {
        private String id;
    }
}
