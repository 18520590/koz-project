package com.koz.common.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PagingDto<T> {
    private long total;
    private int page;
    private int size;
    private List<T> data;
}
