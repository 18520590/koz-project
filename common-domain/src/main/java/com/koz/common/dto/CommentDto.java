package com.koz.common.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

public class CommentDto {
    private CommentDto() {}
    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Request {
        @NotBlank(message = "content is required")
        @Size(min = 1, message = "Length must greater 1 character")
        private String content;
        private String htmlContent;
        @NotBlank(message = "postId is required")
        private String postId;
        private String parentId;
    }
    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Response {
        private String id;
        private String userId;
        private String content;
        private String htmlContent;
        private String postId;
        private String parentId;
        private long sCmtCnt;
        private long rCmtCnt;
        private LocalDateTime createdAt;
        private LocalDateTime updatedAt;
    }

}
