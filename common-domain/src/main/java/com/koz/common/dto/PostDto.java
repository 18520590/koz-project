package com.koz.common.dto;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class PostDto {
    private PostDto() {}
    @Getter
    @Setter
    public static class Request {
        @NotBlank(message = "content is required")
        @Size(min = 1, message = "Length must greater 1 character")
        private String content;
        private String htmlContent;
        @NotNull
        private List<@NotNull String> images = new ArrayList<>();
    }
    @Getter
    @Setter
    public static class Response {
        private String id;
        private String userId;
        private String content;
        private String htmlContent;
        private long rCnt;
        private long cCnt;
        private List<String> images;
        private LocalDateTime createdAt;
        private LocalDateTime updatedAt;
    }

    @Getter
    @Setter
    public static class ResponseV2 extends Response {
        private PagingDtoV2<CommentDto.Response> comments;
    }

}
