package com.koz.common.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthDto {
    private AuthDto() {}

    @Setter
    @Getter
    public static class Request {
        @NotBlank
        @Email
        private String email;
        @NotBlank
        private String password;
    }
    @Getter
    @Setter
    public static class Response {
        private String accessToken;
        private String refreshToken;
    }

    @Getter
    @Setter
    public static class ClaimObject {
        private String userId;
    }
}
