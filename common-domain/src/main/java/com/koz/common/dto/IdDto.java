package com.koz.common.dto;

import lombok.Getter;
import lombok.Setter;

public class IdDto {
    private IdDto() {}
    @Getter
    @Setter
    public static class Response {
        private String id;
    }
}
