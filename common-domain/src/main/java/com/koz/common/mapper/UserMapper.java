package com.koz.common.mapper;


import com.koz.common.domain.User;
import com.koz.common.dto.RegisterDto;
import com.koz.common.dto.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    User toEntity(UserDto userDto);

    UserDto toDto(User user);

    User toEntity(RegisterDto.Request registerDto);
}
