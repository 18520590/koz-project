package com.koz.common.mapper;

import com.koz.common.domain.Comment;
import com.koz.common.dto.CommentDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CommentMapper {
    CommentMapper INSTANCE = Mappers.getMapper(CommentMapper.class);

    Comment toEntity(CommentDto.Request request);

    CommentDto.Response toDto(Comment comment);
}