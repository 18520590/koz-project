package com.koz.common.mapper;


import com.koz.common.domain.Post;
import com.koz.common.dto.PostDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PostMapper {
    PostMapper INSTANCE = Mappers.getMapper(PostMapper.class);

    Post toEntity(PostDto.Request request);

    PostDto.Response toDto(Post post);
}
