package com.koz.common.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document("comments")
@Getter
@Setter
@FieldNameConstants
public class Comment {
    @Id
    private String id;
    private String userId;
    private String postId;
    private String parentId;
    private String content;
    private String htmlContent;
    private long sCmtCnt;
    private long rCmtCnt;
    @CreatedDate
    private LocalDateTime createdAt;
    @LastModifiedDate
    private LocalDateTime updatedAt;
}
