package com.koz.common.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Document("posts")
@Getter
@Setter
@FieldNameConstants
public class Post {
    @Id
    private String id;
    private String userId;
    private String content;
    private String htmlContent;
    private List<String> images;
    private long rCnt;
    private long cCnt;
    @CreatedDate
    private LocalDateTime createdAt;
    @LastModifiedDate
    private LocalDateTime updatedAt;
}
