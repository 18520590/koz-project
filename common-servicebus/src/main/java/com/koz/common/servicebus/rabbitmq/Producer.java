package com.koz.common.servicebus.rabbitmq;


import com.koz.common.messaging.SNSMessage;

public interface Producer {
    void publish(SNSMessage payload, String exchange, String routingKey);
}
