package com.koz.common.servicebus.rabbitmq;

public class Constant {
    private Constant(){}

    // Define exchange constant
    public static final String TOPIC_EXCHANGE = "snsbackend.topic-exchange";

    // Define routing constant

    public static final String RT_COMMENT_ACTION = "comment.action.*";


    public static final String COMMENT_QUEUE_NAME = "comment.action.queue";
}
