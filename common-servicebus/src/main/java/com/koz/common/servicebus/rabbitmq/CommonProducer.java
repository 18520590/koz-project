package com.koz.common.servicebus.rabbitmq;

import com.koz.common.messaging.SNSMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CommonProducer implements Producer {

    private final AmqpTemplate amqpTemplate;

    public CommonProducer(AmqpTemplate amqpTemplate) {
        this.amqpTemplate = amqpTemplate;
    }

    @Override
    public void publish(SNSMessage message, String exchange, String routingKey) {
        log.info("Publishing to {} using routingKey {}. Payload: {}", exchange, routingKey, message);
        amqpTemplate.convertAndSend(exchange, routingKey, message);
        log.info("Published to {} using routingKey {}. Payload: {}", exchange, routingKey, message);
    }
}