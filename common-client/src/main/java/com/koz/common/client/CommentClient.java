package com.koz.common.client;


import com.koz.common.dto.CommentDto;
import com.koz.common.dto.IdDto;
import com.koz.common.dto.PagingDtoV2;
import jakarta.validation.Valid;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@FeignClient(
        name = "comment",
        url = "${clients.comment-service.url}"
)
public interface CommentClient {
    @GetMapping(path = "/v1/comments/{id}")
    ResponseEntity<CommentDto.Response> getById(@PathVariable(value = "id")String commendId);

    @GetMapping("/v1/comments/by-post/{post-id}")
    ResponseEntity<PagingDtoV2<CommentDto.Response>> getCommentsByPostId(
            @PathVariable(value = "post-id")String postId,
            @RequestParam(name = "last-id", required = false)String lastId,
            @RequestParam(name = "limit", required = false, defaultValue = "10")Integer limit);

    @PostMapping("/v1/comments")
    ResponseEntity<IdDto.Response> create(@Valid @RequestBody CommentDto.Request comment,
                                          @RequestHeader("userId")String userId);

    @DeleteMapping("/v1/comments/{id}")
    ResponseEntity<Void> deleteComment(@RequestHeader("userId")String userId,
                                              @PathVariable(value = "id")String commentId);

}