package com.koz.common.client;


import com.koz.common.dto.PostDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(
        name = "post",
        url = "${clients.post-service.url}"
)
public interface PostClient {
    @GetMapping("/v1/posts/{id}")
    ResponseEntity<PostDto.Response> getById(@PathVariable(value = "id")String postId);
}
