package com.koz.commentservice.service.impl;


import com.koz.commentservice.repository.CommentRepository;
import com.koz.commentservice.repository.CommentRepositoryCustom;
import com.koz.commentservice.service.CommentService;
import com.koz.common.client.PostClient;
import com.koz.common.domain.Comment;
import com.koz.common.dto.CommentDto;
import com.koz.common.dto.IdDto;
import com.koz.common.dto.PagingDtoV2;
import com.koz.common.exception.ApiException;
import com.koz.common.exception.ExceptionEnum;
import com.koz.common.mapper.CommentMapper;
import com.koz.common.messaging.Action;
import com.koz.common.messaging.CommentMessage;
import com.koz.common.servicebus.rabbitmq.Constant;
import com.koz.common.servicebus.rabbitmq.Producer;
import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CommentServiceImpl implements CommentService {
    private final CommentRepository commentRepository;
    private final CommentRepositoryCustom commentRepositoryCustom;
    private final PostClient postClient;
    private final Producer producer;
    private static final CommentMapper commentMapper = CommentMapper.INSTANCE;
    private static final int MAX_FETCH = 100;

    public CommentServiceImpl(CommentRepository commentRepository, CommentRepositoryCustom commentRepositoryCustom,
                              PostClient postClient, Producer producer) {
        this.commentRepository = commentRepository;
        this.commentRepositoryCustom = commentRepositoryCustom;
        this.postClient = postClient;
        this.producer = producer;
    }

    @Override
    public IdDto.Response create(CommentDto.Request comment, String userId) {
        log.info("Start create comment, userId={}", userId);
        try {
             postClient.getById(comment.getPostId()); // Post exist, check isSubComment
        }
        catch (FeignException.NotFound ex) {
            throw new ApiException(ExceptionEnum.NOT_FOUND, "Post doesn't exist.", ex);
        }
        catch (Exception ex) {
            throw new ApiException(ExceptionEnum.UNKNOWN_ERROR, "Processing error!", ex);
        }
        Comment commentEnt = commentMapper.toEntity(comment);
        commentEnt.setUserId(userId);
        commentEnt.setRCmtCnt(0);
        commentEnt.setSCmtCnt(0);
        commentRepository.save(commentEnt);
        var baseDto = new IdDto.Response();
        baseDto.setId(commentEnt.getId());
        String parentId = Strings.isNotBlank(commentEnt.getParentId())? commentEnt.getParentId(): null;
        CommentMessage commentMsg = CommentMessage
                .builder()
                .id(commentEnt.getId())
                .action(Action.ADD)
                .postId(commentEnt.getPostId())
                .parentId(parentId)
                .build();

        producer.publish(commentMsg,
                Constant.TOPIC_EXCHANGE,
                Constant.RT_COMMENT_ACTION);
        return baseDto;
    }

    @Override
    public boolean delete(String commentId, String userId) {
        var deletedCount = commentRepository.deleteCommentByIdAndUserId(commentId, userId);
        return deletedCount > 0;
    }

    @Override
    public CommentDto.Response getById(String commentId) {
        var comment = commentRepository.findById(commentId);
        return comment
                .map(commentMapper::toDto)
                .orElse(null);
    }

    @Override
    public PagingDtoV2<CommentDto.Response> getCommentsByPostId(String postId, String lastId, int limit) {
        if(limit <= 0 || limit > MAX_FETCH) {
            limit = MAX_FETCH;
        }
        var comments = commentRepositoryCustom.getCommentsByPostId(postId, lastId, limit);
        int total = comments.size();
        String newLastId = null;
        if(total != 0) {
            newLastId = comments.get(total - 1).getId();
        }

        var result = new PagingDtoV2<CommentDto.Response>();
        var commentsDto = comments.stream().map(commentMapper::toDto).toList();
        result.setData(commentsDto);
        result.setLastId(newLastId);
        result.setTotal(total);
        return result;
    }
}
