package com.koz.commentservice.service;

import com.koz.common.dto.CommentDto;
import com.koz.common.dto.IdDto;
import com.koz.common.dto.PagingDtoV2;

public interface CommentService {
    IdDto.Response create(CommentDto.Request comment, String userId);

    boolean delete(String comment, String userId);

    CommentDto.Response getById(String comment);

    PagingDtoV2<CommentDto.Response> getCommentsByPostId(String postId, String lastId, int limit);
}
