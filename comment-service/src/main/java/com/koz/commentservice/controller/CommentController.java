package com.koz.commentservice.controller;

import com.koz.commentservice.service.CommentService;
import com.koz.common.dto.CommentDto;
import com.koz.common.dto.IdDto;
import com.koz.common.dto.PagingDtoV2;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/comments")
@Slf4j
public class CommentController {
    private final CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<CommentDto.Response> getById(@PathVariable(value = "id")String commendId) {
        var comment = commentService.getById(commendId);
        return new ResponseEntity<>(comment, HttpStatus.OK);
    }

    @GetMapping("/by-post/{post-id}")
    public ResponseEntity<PagingDtoV2<CommentDto.Response>> getCommentsByPostId(
                                                            @PathVariable(value = "post-id")String postId,
                                                           @RequestParam(name = "last-id", required = false)String lastId,
                                                           @RequestParam(name = "limit", required = false, defaultValue = "10")Integer limit) {
        log.info("Receive get comments by post request with postId={}, lastId={}, limit={}", postId, lastId, limit);
        var comments = commentService.getCommentsByPostId(postId, lastId, limit);
        return new ResponseEntity<>(comments, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<IdDto.Response> create(@Valid @RequestBody CommentDto.Request comment,
                                                 @RequestHeader("userId")String userId) {
        log.info("Receive post request to create new comment with userId={}, postId={}, parentCmtId={}", userId, comment.getPostId(), comment.getParentId());
        var response = commentService.create(comment, userId);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteComment(@RequestHeader("userId")String userId,
                                                @PathVariable(value = "id")String commentId) {
        log.info("Receive delete request comment with userId={}, commentId={}", userId, commentId);
        var success = commentService.delete(commentId, userId);
        if(success) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
