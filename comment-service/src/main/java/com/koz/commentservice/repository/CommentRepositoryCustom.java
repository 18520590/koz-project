package com.koz.commentservice.repository;



import com.koz.common.domain.Comment;

import java.util.List;

public interface CommentRepositoryCustom {
    List<Comment> getCommentsByPostId(String postId, String lastId, int limit);

}
