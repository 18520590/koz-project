package com.koz.commentservice.repository.impl;

import com.koz.commentservice.repository.CommentRepositoryCustom;
import com.koz.common.domain.Comment;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

@Repository
public class CommentRepositoryImpl implements CommentRepositoryCustom {
    private final MongoOperations mongoOperations;

    public CommentRepositoryImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<Comment> getCommentsByPostId(String postId, String lastId, int limit) {
        Query query = new Query();
        if(Objects.nonNull(lastId)) {
           query.addCriteria(Criteria.where(Comment.Fields.id).lt(new ObjectId(lastId)));
        }
        query.addCriteria(Criteria.where(Comment.Fields.postId).is(postId));
        query.with(Sort.by(Comment.Fields.id).descending());
        query.limit(limit);
        return mongoOperations.find(query, Comment.class);
    }
}
