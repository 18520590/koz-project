package com.koz.commentservice.repository;


import com.koz.common.domain.Comment;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CommentRepository extends MongoRepository<Comment, String> {
    long deleteCommentByIdAndUserId(String id, String userId);

}
