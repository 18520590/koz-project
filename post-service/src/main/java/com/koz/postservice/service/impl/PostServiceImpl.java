package com.koz.postservice.service.impl;

import com.koz.common.client.CommentClient;
import com.koz.common.domain.Post;
import com.koz.common.dto.IdDto;
import com.koz.common.dto.PagingDto;
import com.koz.common.dto.PostDto;
import com.koz.common.exception.ApiException;
import com.koz.common.exception.ExceptionEnum;
import com.koz.common.mapper.PostMapper;
import com.koz.postservice.repository.PostRepository;
import com.koz.postservice.repository.PostRepositoryCustom;
import com.koz.postservice.service.PostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PostServiceImpl implements PostService {
    private final PostRepository postRepository;
    private final PostRepositoryCustom postRepositoryCustom;
    private final CommentClient commentClient;
    private static final PostMapper postMapper = PostMapper.INSTANCE;
    private static final int MAX_FETCH = 10;

    public PostServiceImpl(PostRepository postRepository, PostRepositoryCustom postRepositoryCustom, CommentClient commentClient) {
        this.postRepository = postRepository;
        this.postRepositoryCustom = postRepositoryCustom;
        this.commentClient = commentClient;
    }

    @Override
    public IdDto.Response create(PostDto.Request post, String userId) {
        log.info("Post service create with userId={}", userId);
        Post postEnt = postMapper.toEntity(post);
        postEnt.setUserId(userId);
        postEnt.setRCnt(0);
        postRepository.save(postEnt);
        var baseDto = new IdDto.Response();
        baseDto.setId(postEnt.getId());
        return baseDto;
    }

    @Override
    public void update(PostDto.Request post, String userId, String postId) {
        log.info("Post service update with userId={}, postId={}", userId, postId);
           var optionalPost = postRepository.findByIdAndUserId(postId, userId);
           if(optionalPost.isEmpty()){
               return;
           }
           var updatedPost = new Post();
           //Dont use beanUtils(it uses reflect)
           BeanUtils.copyProperties(optionalPost.get(), updatedPost);
           BeanUtils.copyProperties(post, updatedPost);
           postRepository.save(updatedPost);
    }

    @Override
    public boolean delete(String postId, String userId) {
        log.info("Post service delete with userId={}, postId={}", userId, postId);
        var deletedCount = postRepository.deletePostByIdAndUserId(postId, userId);
        return deletedCount > 0;
    }

    @Override
    public PostDto.Response getById(String postId) {
        log.info("Post service get by postId={}", postId);
        var post = postRepository.findById(postId);
        if(post.isEmpty()) {
            throw new ApiException(ExceptionEnum.ENTITY_NOT_FOUND);
        }
        return postMapper.toDto(post.get());
    }

    @Override
    public PagingDto<PostDto.Response> findAllByUserId(String userId, int pageSize, int limit) {
        log.info("Post service get by userId={} pageSize={}, limit={}", userId, pageSize, limit);
        Pageable pageable = PageRequest.of(pageSize, limit,
                Sort.by(Post.Fields.createdAt).descending());
        var posts = postRepository.findAllByUserId(userId, pageable);
        var pagingDto = new PagingDto<PostDto.Response>();
        pagingDto.setPage(pageSize);
        pagingDto.setSize(limit);
        pagingDto.setTotal(posts.getTotalElements());
        pagingDto.setData(posts
                .getContent()
                .stream()
                .map(postMapper::toDto)
                .toList());
        return pagingDto;
    }

    @Override
    public long incValueDocument(String postId, String field, int value) {
        log.info("Plus or minus value of postId={} has field={} is {}", postId, field, value);
        return postRepositoryCustom.incValueDocument(postId, field, value);
    }

}
