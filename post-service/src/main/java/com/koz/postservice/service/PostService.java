package com.koz.postservice.service;


import com.koz.common.dto.IdDto;
import com.koz.common.dto.PagingDto;
import com.koz.common.dto.PostDto;

public interface PostService {
    IdDto.Response create(PostDto.Request post, String userId);

    void update(PostDto.Request post, String userId, String postId);

    boolean delete(String postId, String userId);

    PostDto.Response getById(String postId);

    PagingDto<PostDto.Response> findAllByUserId(String userId, int pageSize, int limit);

    long incValueDocument(String postId, String field, int value);
}
