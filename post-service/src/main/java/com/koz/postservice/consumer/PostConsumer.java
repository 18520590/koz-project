package com.koz.postservice.consumer;


import com.koz.common.domain.Post;
import com.koz.common.messaging.Action;
import com.koz.common.messaging.CommentMessage;
import com.koz.common.messaging.SNSMessage;
import com.koz.common.servicebus.rabbitmq.Constant;
import com.koz.postservice.service.PostService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@Slf4j
public class PostConsumer {
    private final PostService postService;
    private static final int PLUS_ONE = 1;
    private static final int MINUS_ONE = 1;

    @RabbitListener(queues = Constant.COMMENT_QUEUE_NAME)
    public void consumer(SNSMessage snsMessage) {
        log.info("Received message from , messageId = {}", snsMessage.getId());
        if(snsMessage instanceof CommentMessage commentMessage) {
            Action action = commentMessage.getAction();
            switch (action) {
                case ADD -> {
                   long total =  postService.incValueDocument(commentMessage.getPostId(),
                            Post.Fields.cCnt, PLUS_ONE);
                    log.info("Increase post count successful. Total documents update = {}", total);
                }
                case REMOVE -> {
                    long total = postService.incValueDocument(commentMessage.getPostId(),
                            Post.Fields.cCnt, MINUS_ONE);
                    log.info("Decrease post count successful. Total documents update = {}", total);
                }
                default -> log.warn("Action {} not supported!", action.name());
            }
        }
    }
}
