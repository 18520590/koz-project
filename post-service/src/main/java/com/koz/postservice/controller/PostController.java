package com.koz.postservice.controller;


import com.koz.common.dto.IdDto;
import com.koz.common.dto.PagingDto;
import com.koz.common.dto.PostDto;
import com.koz.postservice.service.PostService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/posts")
@Slf4j
public class PostController {
    private final PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping()
    public PagingDto<PostDto.Response> getPaging(@RequestHeader("userId") @NotEmpty String userId,
                                                 @RequestParam @Min(value = 0, message = "Page must be greater or equal 0")int page,
                                                 @RequestParam @Min(0) @Max(value = 100, message = "Size must in range 0 - 100")int size) {
        return postService.findAllByUserId(userId, page, size);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PostDto.Response> getById(@PathVariable(value = "id")String postId) {
        var post = postService.getById(postId);
        return new ResponseEntity<>(post, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<IdDto.Response> createPost(@Valid @RequestBody PostDto.Request post,
                                                     @RequestHeader("userId")String userId) {
        log.info("Starting create post from userId={}", userId);
        var response = postService.create(post, userId);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public void updatePost(@RequestHeader("userId")String userId,
                           @PathVariable(value = "id")String postId,
                           @RequestBody @Valid PostDto.Request post){
        postService.update(post, userId, postId);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePost(@RequestHeader("userId")String userId,
                                     @PathVariable(value = "id")String postId) {
        log.info("Starting delete postId={} from userId={}", postId, userId);
        var success = postService.delete(postId, userId);
        if(success) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
