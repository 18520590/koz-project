package com.koz.postservice.repository;

import com.koz.common.domain.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface PostRepository extends MongoRepository<Post, String> {
    long deletePostByIdAndUserId(String id, String userId);
    Page<Post> findAllByUserId(String userId, Pageable pageable);
    Optional<Post> findByIdAndUserId(String id, String userId);
}
