package com.koz.postservice.repository.impl;

import com.koz.common.domain.Post;
import com.koz.postservice.repository.PostRepositoryCustom;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

@Repository
public class PostRepositoryCustomImpl implements PostRepositoryCustom {

    private final MongoOperations mongoOperations;

    public PostRepositoryCustomImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public long incValueDocument(String postId, String field, int value) {
        Query query = Query.query(Criteria.where(Post.Fields.id).is(postId));
        Update update = new Update().inc(field, value);
        var result  = mongoOperations.updateFirst(query, update, Post.class);
        return result.getModifiedCount();
    }
}
