package com.koz.postservice.repository;

public interface PostRepositoryCustom {
    long incValueDocument(String postId, String field, int value);
}
